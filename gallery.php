<!DOCTYPE html>
<html lang="en">
<head>
    <title>FitLine | Programs</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet"
          charset="utf-8"/>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto();
        });
    </script>

</head>


<body id="page4">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div id="main" class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3 class="prev-indent-bot">گالری تصاویر</h3>
                        <div class="wrapper">
                            <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="about">
                                    <figure>
                                        <div class="gallery clearfix">
                                            <a href="images/g3.jpg" rel="prettyPhoto[gallery1]" title="single photo">
                                                <img src="images/g3.jpg" style="width:100%; height:auto;">
                                            </a>
                                        </div>
                                        <div class="about-text">
                                            <h4> آلبوم اول</h4>

                                        </div>
                                    </figure>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="about">
                                    <figure>
                                        <div class="gallery clearfix">
                                            <a href="images/g4.jpg" rel="prettyPhoto[gallery1]" title="single photo">
                                                <img src="images/g4.jpg" style="width:100%; height:auto;">
                                            </a>
                                        </div>
                                        <div class="about-text">
                                            <h4> آلبوم دوم</h4>

                                        </div>
                                    </figure>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="about">
                                    <figure>
                                        <div class="gallery clearfix">
                                            <a href="images/g5.jpg" rel="prettyPhoto[gallery1]" title="single photo">
                                                <img src="images/g5.jpg" style="width:100%; height:auto;">
                                            </a>
                                        </div>
                                        <div class="about-text">
                                            <h4> آلبوم سوم</h4>

                                        </div>
                                    </figure>
                                </div>
                            </div>

                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";

?>

</body>
</html>

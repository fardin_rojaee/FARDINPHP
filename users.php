<!DOCTYPE html>
<html lang="en">
<head>
    <title>FitLine | Programs</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet"
          charset="utf-8"/>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto();
        });
    </script>
    <style>

        table {
            border:1px solid orange;
            width: 100%;
        }
        tr:not(:last-child) {
            border-bottom: 1px solid red;
        }

    </style>



</head>


<body id="page4">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div id="main" class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3 class="prev-indent-bot">لیست اعضاء</h3>
                        <table>
                            <?php
                            $conn = new mysqli("localhost", "fardin", "W1cv3etQNiAQ4SqW", "school");
                            if ($conn->connect_error) {
                                die("connection failed:" . $conn->connect_error);

                            } else {
                                $select = "SELECT * FROM users";
                                mysqli_set_charset($conn,"utf8");
                                $select = mysqli_query($conn, $select);
                                if (mysqli_num_rows($select) > 0) {
                                    while ($user = mysqli_fetch_assoc($select)) {
                                        $name = $user['name'];
                                        $userName = $user['username'];
                                        $password = $user['password'];

                                        echo "<tr><td>$name</td><td>$userName</td><td>$password</td></tr>";
                                    }
                                }
                            }


                            ?>

                        </table>


                    </article>
                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";

?>

</body>
</html>

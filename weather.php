<!DOCTYPETemperaturetml>
<head>
    <title>FitLine | Contacts</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
</head>
<body id="page5">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3>جستجوی آب و هوا</h3>
                        <form id="form-movies" action="?" method="post">
                            <fieldset>
                                <input type="hidden" name="action" value="weather">
                                <input type="text" name="moviesword"
                                       placeholder="نام فیلم مورد نظر را به لاتین وارد کنید" style="width: 40%; font:12px Tahoma;height: 30px;
                                       outline: none;border: 2px solid orange;padding: 5px;">
                                <div class="extra-wrap">
                                    <div class="buttons"><input type="submit" value="ارسال" class="button-2"></div>
                                </div>
                            </fieldset>
                        </form>
                    </article>
                    <br><br>
                    <?php
                    $action = $_POST['action'];

                    if ($action == 'weather') {
                        $weatherWord = $_POST['moviesword'];
                        $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22$weatherWord%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
                        $result = file_get_contents($url);
                        $weatherArray = json_decode($result, true);

                        echo "<div>"."The location is:"."<b>".$weatherArray['query']['results']['channel']['location']['city']."</b>"."</div>";
                        echo "<div>"."The Temp is:"."<b>".$weatherArray['query']['results']['channel']['item']['condition']['temp']."</b>"."</div>";
                        $weatherLink=$weatherArray['query']['results']['channel']['image']['link'];
                        echo "<div>"."The Link is:"."<b><a href='$weatherLink' target='_blank'>$weatherLink</a></b>"."</div>";



                    }


                    ?>

                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";
?>
</body>
</html>

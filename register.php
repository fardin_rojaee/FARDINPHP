<!DOCTYPE html>
<html lang="en">
<head>
    <title>FitLine | Diets</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
    <style>

         input[type="text"],
         input[type="password"]{
            width: 25%;
            height: 30px;
            padding: 5px;
            border: 1px solid orange;
            outline: none;
            font-family: Tahoma;
        }
         input[type="submit"]{
             font-family: Tahoma;
             background-color: orange;
             color: white;
             font-family: Tahoma;
             border: 1px solid white;
             padding: 5px 10px;
             height: 40px;
             display: inline-block;
             cursor: pointer;
         }


    </style>


</head>
<body id="page3">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="Search"
                                   onBlur="if(this.value=='') this.value='Search'"
                                   onFocus="if(this.value =='Search' ) this.value=''"/>
                            <a class="search-button" href="#"></a></div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3 class="prev-indent-bot">فرم ثبت نام دانش آموزان</h3>
                        <form action="destenition.php" method="post">
                            <input type="hidden" name="action" value="register">
                            <input type="text" name="fullname" placeholder="نام و نام خانوادگی">
                            <input type="text" name="username" placeholder="نام کاربری">
                            <input type="password" name="password" placeholder="پسورد">
                            <input type="submit" value="ارسال">
                        </form>


                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->

<?php

include "footer.html";
?>
</body>
</html>

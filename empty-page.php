<!DOCTYPE html>
<html lang="en">
<head>
    <title>FitLine | Nutrition</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
</head>
<body id="page2">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div class="main">
                <?php
                include "header.html";

                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3>عنوان متن</h3>
                        <div class="wrapper">
                            <div class="extra-wrap">
                                <p class="p3"> خوشبختانه امروزه ایمپلنت دندان ، روبرو شدن با مشکلات بی دندانی را بسیار
                                    آسان کرده است.
                                    از دست دادن یک یا چند دندان، مشکلی است که برای بسیاری از انسانها سخت و حتی غیرقابل
                                    تحمل است،
                                    اما به دلایل متعدد احتمال روبرو شدن با عارضه ی بی دندانی وجود دارد.
                                    مشکلی که تا چندی پیش راهی جز استفاده از دتدان های مصنوعی نداشت.
                                    این دندان ها به دلیل شکل ظاهری و مشکلات دیگری که داشت، رضایت مریض را جلب نمی کرد.
                                    ایمپلنتولوژی حاصل آخرین تحقیقات و تکنولوژی های روز دنیا است.
                                    قرار دادن ایمپلنت دندان در فک، درمانی نه چندان سخت است که عارضه ای در بر ندارد.
                                    ایمپلنت دندانی همان نقش دندان های اصلی را تا پایان عمر برای بیمار ایفا می کند.
                                    از بین روش های مختلف جایگزینی ریشه دندان، ایمپلنت دندان جایگزینی کامل محسوب میشوند،
                                    چراکه هم ریشه و هم تاج دندان را شامل می شوند..

                                </p>
                            </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";
?>
</body>
</html>

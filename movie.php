<!DOCTYPE html>
<html>
<head>
    <title>FitLine | Contacts</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
</head>
<body id="page5">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row-bot"></div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_12">
                        <h3>جستجوی فیلم های سینمایی</h3>
                        <form id="form-movies" action="?" method="post">
                            <fieldset>
                                <input type="hidden" name="action" value="movies">
                                <input type="text" name="moviesword"
                                       placeholder="نام فیلم مورد نظر را به لاتین وارد کنید" style="width: 40%; font:12px Tahoma;height: 30px;
                                       outline: none;border: 2px solid orange;padding: 5px;">
                                <div class="extra-wrap">
                                    <div class="buttons"><input type="submit" value="ارسال" class="button-2"></div>
                                </div>
                            </fieldset>
                        </form>
                    </article>
                    <br><br>
                    <?php
                    $action = $_POST['action'];

                    if ($action == 'movies') {
                        $moviesWord = $_POST['moviesword'];
                        $url = "http://www.omdbapi.com/?t=$moviesWord&apikey=2bd97486";
                        $result = file_get_contents($url);
                        $movieArray = json_decode($result, true);
                        echo "<div style='text-align: center;font-size: 20px;'>Title is :" . " " . $movieArray['Title'] . "</div>";
                        echo "<div style='text-align: center;font-size: 10px;'>Year is " . " " . $movieArray['Year'] . "</div>";
                        echo "<div style='text-align: center;font-size: 15px;'>Actors are:" . "<b>" . $movieArray['Actors'] . "</b></div>";

                        $poster = $movieArray['Poster'];

                        echo "<img style='width: 30%; display: block;margin: auto;' src='$poster'>";

                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";
?>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>FitLine</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/component.css">
    <script src="js/jquery-1.6.3.min.js" type="text/javascript"></script>
    <script src="js/tabs.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
</head>
<body id="page1">
<div class="bg">
    <!--==============================header=================================-->
    <header>
        <div class="menu-row">
            <div class="main">
                <?php
                include "header.html";
                ?>
            </div>
        </div>
        <div class="main">
            <div class="wrapper p3">
                <h1><a href="index.html">FitLine</a></h1>
                <form id="search-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="search-field">
                            <input name="search" type="text" value="جستجو..." onClick="this.value=''"
                                   onBlur="if(this.value=='') {this.value='جستجو...'}"/>
                            <a class="search-button" href="#"></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="tab_container">
            <article id="cc-slider">
                <input checked="checked" name="cc-slider" id="slide1" type="radio">
                <input name="cc-slider" id="slide2" type="radio">
                <input name="cc-slider" id="slide3" type="radio">
                <input name="cc-slider" id="slide4" type="radio">
                <input name="cc-slider" id="slide5" type="radio">

                <div id="cc-slides">
                    <div id="overflow">
                        <div class="inner">
                            <article>
                                <div class="cctooltip">
                                    <h3><a href="#">ادامه مطلب</a>...</h3>
                                </div>
                                <img src="images/slide-1.jpg">
                            </article>
                            <article>
                                <div class="cctooltip">
                                    <h3><a href="#">ادامه مطلب</a>...</h3>
                                </div>
                                <img src="images/slide-2.jpg"></article>
                            <article>
                                <div class="cctooltip">
                                    <h3><a href="#">ادامه مطلب</a>...</h3>
                                </div>
                                <img src="images/slide-3.jpg">
                            </article>
                            <article>
                                <div class="cctooltip">
                                    <h3><a href="#">ادامه مطلب</a>...</h3>
                                </div>
                                <img src="images/slide-4.jpg">
                            </article>
                            <article>
                                <div class="cctooltip">
                                    <h3><a href="#">ادامه مطلب</a>...</h3>
                                </div>
                                <img src="images/slide-5.jpg">
                            </article>
                        </div>
                    </div>
                </div>

                <div id="controls">
                    <label for="slide1"></label>
                    <label for="slide2"></label>
                    <label for="slide3"></label>
                    <label for="slide4"></label>
                    <label for="slide5"></label>
                </div>
            </article>
        </div>
    </header>
    <!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_12">
                <div class="wrapper img-indent-bot">
                    <article class="grid_4 service">
                        <h3>خدمات</h3>
                        <ul class="list-1">
                            <li><a href="#">خدمات ترمیمی</a></li>
                            <li><a href="#">خدمات جراحی</a></li>
                            <li><a href="#">خدمات زیبایی</a></li>
                            <li><a href="#">خدمات درمان ریشه</a></li>
                            <li><a href="#">خدمات پروتز</a></li>
                            <li><a href="#">خدمات اندو</a></li>


                        </ul>
                    </article>
                    <article class="grid_8 online">
                        <h2>تعیین وقت آنلاین</h2>
                        <div class="wrapper prev-indent-bot">
                            <form id="online-form" action="arragment.php" method="post">
                                <fieldset>
                                    <div style="float:right; width:50%;">
                                        <input type="text" name="fullname" placeholder="نام و نام خانوادگی">
                                        <input type="text" name="phone" placeholder="تلفن ثابت">

                                        <input type="text" name="date1" placeholder="انتخاب روز اول"
                                               style="width:42%;">

                                        <select style="width:48%; padding:8px;">
                                            <option>از ساعت 10 الی 12</option>
                                            <option>از ساعت 12 الی 14</option>
                                            <option>از ساعت 16 الی 18</option>
                                        </select>

                                        <input type="text" name="date2" placeholder="انتخاب روز دوم"
                                        style="width:42%;">

                                        <select style="width:48%; padding:8px;">
                                            <option>از ساعت 10 الی 12</option>
                                            <option>از ساعت 12 الی 14</option>
                                            <option>از ساعت 16 الی 18</option>
                                        </select>

                                    </div>
                                    <div style="float:right; width:50%;">
                                        <input type="text" name="mobile" placeholder="موبایل">
                                        <textarea name="commant"></textarea>
                                    </div>

                                </fieldset>

                                <div class="center-btn">
                                    <input type="submit" value="ثبت درخواست" class="button-2">
                                </div>

                            </form>

                        </div>
                    </article>
                </div>
                <div class="wrapper">
                    <article class="grid_4 gride-right">
                        <div class="indent-left">
                            <h3 class="prev-indent-bot">مقالات</h3>
                            <div class="wrapper p2">
                                <figure class="img-indent2"><img src="images/page1-img5.jpg" alt=""></figure>
                                <div class="extra-wrap">
                                    <h6 class="p0">مقالات دندانپزشکی</h6>
                                    مقاله در زمینه تنبی کودکان با عنوان: آیا تنبیه بدنی بسیار بد میباشد؟
                                    <a class="link" href="#"> ادامه مطلب</a></div>
                            </div>
                            <div class="wrapper p2">
                                <figure class="img-indent2"><img src="images/page1-img6.jpg" alt=""></figure>
                                <div class="extra-wrap">
                                    <h6 class="p0">مقالات پزشکی</h6>
                                    مقاله در زمینه تنبی کودکان با عنوان: آیا تنبیه بدنی بسیار بد میباشد؟
                                    <a class="link" href="#"> ادامه مطلب</a></div>
                            </div>
                            <div class="wrapper">
                                <figure class="img-indent2"><img src="images/page1-img7.jpg" alt=""></figure>
                                <div class="extra-wrap">
                                    <h6 class="p0">مقالات مربوط به زیبایی</h6>
                                    مقاله در زمینه تنبی کودکان با عنوان: آیا تنبیه بدنی بسیار بد میباشد؟
                                    <a class="link" href="#"> ادامه مطلب</a></div>
                            </div>
                        </div>
                    </article>


                    <article class="grid_5 gride-right">
                        <h3>آخرین خبر</h3>
                        <div class="wrapper prev-indent-bot">
                            <figure class="img-indent2" style="float:none;">

                                <img src="images/page1-img4.jpg" class="news-img" alt="">

                            </figure>
                            <div class="extra-wrap extra-wrap-news">
                                دندان جزو سیستم سلامتی بدن است . دندان بطور مستقیم یا غیر مستقیم می تواند روی سلامتی
                                انسان اثر بگذارد
                            </div>
                        </div>
                        <div class="center-btn">
                            <a class="button-2" href="#">ادامه مطلب</a>
                        </div>
                    </article>


                    <article class="grid_3 gride-right article-right">
                        <h3 class="prev-indent-bot">لینک های مفید</h3>
                        <ul class="list-1">
                            <li><a href="#">سازمان نظام پزشکی کل کشور</a></li>
                            <li><a href="#">وزارت بهداشت، درمان و آموزش پزشکی</a></li>
                            <li><a href="#">سامانه یکپارچه آموزش مداوم جامعه پزشکی</a></li>
                            <li><a href="#">پرتال اطلاعات پژوهشی پزشکی کشور</a></li>
                            <li><a href="#">وزارت بهداشت، درمان و آموزش پزشکی</a></li>
                            <li><a href="#">سامانه یکپارچه آموزش مداوم جامعه پزشکی</a></li>
                        </ul>
                    </article>
                </div>
            </div>
        </div>
    </section>
</div>
<!--==============================footer=================================-->
<?php
include "footer.html";

?>
</body>
</html>